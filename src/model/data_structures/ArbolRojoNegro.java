package model.data_structures;

import java.util.NoSuchElementException;

public class ArbolRojoNegro<Key extends Comparable<Key>, Value>{
	
	private static final boolean ROJO = true;
	private static final boolean NEGRO = false;
	
	private NodoRN raiz;
	
	private class NodoRN {
		private Key key;
		private Value val;
		private NodoRN izq, der;
		private boolean color;
		private int size;
		
		public NodoRN(Key key, Value val, boolean color, int size ) {
			this.key = key;
			this.val = val;
			this.color = color;
			this.size = size;
		}
	}
	
	public ArbolRojoNegro() {
		raiz = null;
	}
	
	private boolean esRojo(NodoRN x) {
		if(x == null)
			return false;
		return x.color == ROJO;
	}
	
	private int size(NodoRN x) {
		if (x==null)
			return 0;
		return x.size;
	}
	
	public int size() {
		return size(raiz);
	}
	
	public boolean estaVacio() {
		return raiz == null;
	}
	
	/**
	 * Retorna el valor asociado con la llave dada.
	 */
	public Value dar(Key key) {
		if (key == null)
			throw new NullPointerException("argumento dar() es null");
		return dar(raiz, key);
	}
	
	private Value dar(NodoRN x, Key key) {
		while (x != null) {
			int cmp = key.compareTo(x.key);
			if      (cmp < 0) x = x.izq;
			else if (cmp > 0) x = x.der;
			else
				return x.val;
		}
		return null;
	}
	
	public boolean contiene(Key key) {
		return dar(key) !=null;
	}
	
	/**
	 * Insersion
	 */
	public void insertar(Key key, Value val) {
		if (key == null)
			throw new NullPointerException("Primer argumento a insertar() es null");
		if (val == null) {
			eliminar(key);
			return;
		}
		
		raiz = insertar(raiz , key ,val);
		raiz.color = NEGRO;
	}
	
	private NodoRN insertar(NodoRN h, Key key, Value val) {
		if (h == null)
			return new NodoRN(key, val, ROJO,1);
		int cmp = key.compareTo(h.key);
		if      (cmp < 0) h.izq = insertar(h.izq, key, val);
		else if (cmp >0) h.der = insertar( h.der, key, val);
		else
			h.val = val;
		
		if(esRojo(h.der) && !esRojo(h.izq))
			h = rotarIzquierda(h);
		if(esRojo(h.izq) && esRojo(h.izq.izq))
			h = rotarDerecha(h);
		if(esRojo(h.izq) && esRojo(h.der))
			cambiarColor(h);
		h.size = size(h.izq)+ size(h.der)+1;
		
		return h;	
	}
	
	/**
	 * Metodos de eliminacion
	 */
	public void eliminarMin() {
		if (estaVacio()) throw new NoSuchElementException("BST underflow");
		
		if (!esRojo(raiz.izq) && !esRojo(raiz.der))
			raiz.color = ROJO;
		raiz = eliminarMin(raiz);
		if (!estaVacio())
			raiz.color = NEGRO;
	}
	
	private NodoRN eliminarMin(NodoRN h) {
		if (h.izq == null)
			return null;
		
		if (!esRojo(h.izq) && !esRojo(h.izq.izq))
			h = moverRojoIzquierda(h);
		
		h.izq = eliminarMin(h.izq);
		return balancear(h);
	}
	
	public void eliminarMax() {
		if (estaVacio()) throw new NoSuchElementException("BST underflow");
		
		if (!esRojo(raiz.izq) && !esRojo(raiz.der))
			raiz.color = ROJO;
		raiz = eliminarMax(raiz);
		if (!estaVacio())
			raiz.color = NEGRO;
	}
	
	private NodoRN eliminarMax(NodoRN h) {
		if (esRojo(h.izq))
			h = rotarDerecha(h);
		
		if (h.der == null)
			return null;
		
		if (!esRojo(h.der) && !esRojo(h.der.izq))
			h = moverRojoDerecha(h);
		
		h.der = eliminarMax(h.der);
		return balancear(h);
	}
	
	public void eliminar(Key key) {
		if (key == null)
			throw new NullPointerException("argumento a eliminar() es null");
		if (!contiene(key))
			return;
		if (!esRojo(raiz.izq) && !esRojo(raiz.der))
			raiz.color = ROJO;
		
		raiz  = eliminar(raiz, key);
		if (!estaVacio())
			raiz.color = NEGRO;
	}
	
	private NodoRN eliminar(NodoRN h, Key key) {
		if (key.compareTo(h.key) < 0) {
			if (!esRojo(h.izq) && !esRojo(h.izq.izq))
				h = moverRojoIzquierda(h);
			h.izq = eliminar(h.izq, key);
		}
		else {
			if (esRojo(h.izq))
				h = rotarDerecha(h);
			if (key.compareTo(h.key) == 0 && (h.der == null))
				return null;
			if (!esRojo(h.der) && !esRojo(h.der.izq))
				h = moverRojoDerecha(h);
			if (key.compareTo(h.key) == 0) {
				NodoRN x = h.der;
				h.key = x.key;
				h.val = x.val;
				h.der = eliminarMin(h.der);
			}
			else
				h.der = eliminar(h.der, key);
		}
		return balancear(h);
	}
	
	/**
	 * Metodos auxiliares
	 */
	private NodoRN rotarDerecha(NodoRN h) {
		NodoRN x = h.izq;
		h.izq = x.der;
		x.der = h;
		x.color =x.der.color;
		x.der.color = ROJO;
		x.size = h.size;
		h.size = size(h.izq) + size(h.der) + 1;
		return x;
	}
	
	private NodoRN rotarIzquierda(NodoRN h) {
		NodoRN x = h.der;
		h.der = x.izq;
		x.izq = h;
		x.color = x.izq.color;
		x.izq.color = ROJO;
		x.size = h.size;
		h.size = size(h.izq) + size(h.der) +1;
		return x;
	}
	
	private void cambiarColor(NodoRN h) {
		h.color = !h.color;
		h.izq.color = !h.izq.color;
		h.der.color = !h.der.color;
	}
	
	private NodoRN moverRojoIzquierda(NodoRN h) {
		cambiarColor(h);
		if (esRojo(h.der.izq)) {
			h.der = rotarDerecha(h.der);
			h = rotarIzquierda(h);
			cambiarColor(h);
		}
		return h;
	}
	
	private NodoRN moverRojoDerecha(NodoRN h) {
		cambiarColor(h);
		if (esRojo(h.izq.izq)) {
			h = rotarDerecha(h);
			cambiarColor(h);
		}
		return h;
	}
	
	private NodoRN balancear(NodoRN h) {
		if(esRojo(h.der))
			h = rotarIzquierda(h);
		if(esRojo(h.izq) && esRojo(h.izq.izq))
			h = rotarDerecha(h);
		if(esRojo(h.izq) && esRojo(h.der))
			cambiarColor(h);
		
		h.size = size(h.izq) + size(h.der) + 1;
		return h;
	}
	
	/**
	 * Codigo fuente adaptado de la presentacion de clase "Arboles Binarios (Prof Dario Correal)"
	 * disponible en Sicua Plus, Estructura de Datos
	 */
}