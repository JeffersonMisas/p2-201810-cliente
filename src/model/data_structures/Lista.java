package model.data_structures;

import java.util.Iterator;

public class Lista<T extends Comparable<T>> implements IList<T> {

	private int numElementos;
	
	private Nodo<T> primero;
	
	private Nodo<T> ultimo;
	
	public Lista()
	{
		numElementos = 0;
		primero = null;
		ultimo = null;
	}
	
	
	public void add(T elem) {
		Nodo<T> nodo = new Nodo<T>( elem );
		if( primero == null )
		{
			primero = nodo;
			ultimo = nodo;
		}
		else
		{
			ultimo.insertar( nodo );
			ultimo = nodo;
		}
		numElementos++;
		// TODO Auto-generated method stub
		
	}

	public T buscar( T modelo )
	{
		for( Nodo<T> p = primero; p != null; p = p.darSiguiente( ) )
		{
			if( p.darElemento( ).equals( modelo ) )
			{
				return p.darElemento( );
			}
		}
		return null;
	}

	@Override
	public T get(T elem) {
		// TODO Auto-generated method stub
		for( Nodo<T> p = primero; p != null; p = p.darSiguiente( ) )
		{
			if( p.darElemento( ).equals( elem ) )
			{
				return p.darElemento( );
			}
		}
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		
		return new IteratorLista<T>(primero);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return numElementos;
	}

	/**
	 * Retorna el primer nodo de la lista. <br>
	 * <b>post: </b> Se retorn� el primer nodo de la lista.
	 * @return Primer nodo de la lista. Null en caso que la lista sea vac�a.
	 */
}
