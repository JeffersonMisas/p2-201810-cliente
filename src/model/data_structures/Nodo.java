package model.data_structures;

public class Nodo<T> {
	
	private T elemento;
	
	private Nodo<T> siguiente;

	private Nodo<T> anterior;

	
	public Nodo(T pelemento) {
		elemento = pelemento;
		siguiente = null;
		anterior = null;
	}

	public T darElemento() {
		return elemento;
	}

	public Nodo<T> darSiguiente() {
		return siguiente;
	}

	public Nodo<T> darAnterior() {
		return anterior;
	}

	public void insertar(Nodo<T> nodo) {
		nodo.siguiente = siguiente;
		nodo.anterior = this;
		if (siguiente != null)
			siguiente.anterior = nodo;
		siguiente = nodo;
	}
	
	@Override
	public String toString() {
		return elemento.toString();
	}
}
