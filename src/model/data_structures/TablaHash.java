package model.data_structures;

public class TablaHash<Key, Value>
{  
	private static final int CAPACIDAD_INICIAL = 11;
	
	private int m, n;
	private Key[] keys;
	private Value[] values;
	
	public TablaHash() {
		this(CAPACIDAD_INICIAL);
	}
	
	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad) {
		m = capacidad;
		n = 0;
		keys = (Key[])   new Object[m];
		values = (Value[]) new Object[m];
	}
	
	public int size() {
		return n;
	}
	
	public boolean estaVacio() {
		return size() == 0;
	}
	
	public boolean contiene(Key key) {
		if(key == null)
			throw new NullPointerException("argumento para contiene() es null");
		return
				dar(key)!= null;
	}
	
	private int hash(Key key) {
		return
				(key.hashCode() & 0x7fffffff )%m;
	}
	
	public void resize(int capacidad) {
		TablaHash<Key,Value> temp = new TablaHash<Key,Value>(capacidad);
		for (int i = 0; i < m; i++) {
			if (keys[i] != null) {
				temp.insertar(keys[i],values[i]);
			}
		}
		keys = temp.keys;
		values = temp.values;
		m = temp.m;
	}
	
	public void insertar(Key key, Value val) {
		if(key == null)
			throw new NullPointerException("primer argumento a insertar() es null");
		
		if(n>= m/2) resize(2*m);
		
		int i;
		for(i = hash(key);keys[i] != null; i = (i+1)%m) {
			if (keys[i].equals(key)) {
				values[i] = val;
				return;
			}
		}
		keys[i] = key;
		values[i] = val;
		n++;
	}
	
	public Value dar(Key key) {
		if(key == null)
			throw new NullPointerException("argumento a dar() = null");
		for (int i = hash(key);keys[i] != null; i = (i+1) % m )
			if(keys[i].equals(key))
				return values[i];
		return null;
		
	}
}