package model.vo;

public class Servicio implements Comparable<Servicio>{
	
	private String tripId,taxiId,startTime,endTime;
	private int tripSeconds,pickupZone,dropOffZone;
	private double tripMiles, tripTotal, pickupLatitud, pickupLongitud;

	public Servicio(String ptripid, String ptaxiid, String pstarttime, String pendtime, int ptripseconds, int ppickupzone, int pdropoffzone, double ptripmiles, double ptriptotal, double ppickuplatitud, double ppickuplongitud) {
		this.tripId =ptripid;
		this.taxiId = ptaxiid;
		this.startTime = pstarttime;
		this.endTime = pendtime;
		this.tripSeconds = ptripseconds;
		this.pickupZone = ppickupzone;
		this.dropOffZone = pdropoffzone;
		this.tripMiles = ptripmiles;
		this.tripTotal = ptriptotal;
		this.pickupLatitud = ppickuplatitud;
		this.pickupLongitud = ppickuplongitud;
	}
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	public String getStartTime(){
		//TODO Auto-generated method stub
		return startTime;
	}
	
	public String getEndtTime(){
		//TODO Auto-generated method stub
		return endTime;
	}

	public int getPickupZone(){
		//TODO Auto-generated method stub
		return pickupZone;
	}

	public int getDropOffZone(){
		//TODO Auto-generated method stub
		return dropOffZone;
	}


	public double getPickupLatitud(){
		//TODO Auto-generated method stub
		return pickupLatitud;
	}

	public double getPickupLongitud(){
		//TODO Auto-generated method stub
		return pickupLongitud;
	}

	@Override
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return this.getTripId().compareTo( arg0.getTripId() );
	}
}