package model.vo;

public class Taxi implements Comparable<Taxi> {

	private String taxiId,company;
	
	public Taxi(String pId, String pComapany) {
		this.taxiId = pId;
		this.company = pComapany;
	}
    /**
     * @return id - taxi_id
     */
    public String getTaxiId()
    {
        // TODO Auto-generated method stub
        return taxiId;
    }

    /**
     * @return company
     */
    public String getCompany()
    {
        // TODO Auto-generated method stub
        return company;
    }

    @Override
    public int compareTo(Taxi o)
    {
        // TODO Auto-generated method stub
        return this.getTaxiId().compareTo(o.getTaxiId());
    }
}
