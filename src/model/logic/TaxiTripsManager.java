package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.lang.Math;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import API.ITaxiTripsManager;
import model.data_structures.ArbolRojoNegro;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.TablaHash;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class TaxiTripsManager implements ITaxiTripsManager {

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	private String[] jsonLarge = {"./data/taxi-trips-wrvz-psew-subset-large/taxi-trips-wrvz-psew-subset-02-02-2017.json","./data/taxi-trips-wrvz-psew-subset-large/taxi-trips-wrvz-psew-subset-03-02-2017.json","./data/taxi-trips-wrvz-psew-subset-large/taxi-trips-wrvz-psew-subset-04-02-2017.json","./data/taxi-trips-wrvz-psew-subset-large/taxi-trips-wrvz-psew-subset-05-02-2017.json","./data/taxi-trips-wrvz-psew-subset-large/taxi-trips-wrvz-psew-subset-06-02-2017.json","./data/taxi-trips-wrvz-psew-subset-large/taxi-trips-wrvz-psew-subset-07-02-2017.json","./data/taxi-trips-wrvz-psew-subset-large/taxi-trips-wrvz-psew-subset-08-02-2017.json"};

	private Lista<Servicio> serviciosCargados = new Lista<Servicio>();
	private Lista<Taxi> taxisCargados = new Lista<Taxi>();
	private ArbolRojoNegro<String, Lista<String>> companiaTaxis = new ArbolRojoNegro<>();
	private TablaHash<Integer,Lista<Servicio>> zonaInicioServicios = new TablaHash<>();
	private TablaHash<Integer,Lista<Servicio>> rangoDuracionServicios = new TablaHash<>();
	private ArbolRojoNegro<Double, Lista<Servicio>> millaServicios = new ArbolRojoNegro<>();
	private TablaHash<String, ArbolRojoNegro<String, Lista<Servicio>>> rangoZonaServicios = new TablaHash<>();
	private double latitudRef;
	private double longitudRef;

	@Override
	public boolean cargarSistema(String direccionJson) {

		JsonParser parser = new JsonParser();

		if (direccionJson.equals(DIRECCION_LARGE_JSON)) {
			try {
				for(int j = 0;j<jsonLarge.length;j++) {
					JsonArray jsonA= (JsonArray) parser.parse(new FileReader(jsonLarge[j]));

					for( int i =0; jsonA != null && i < jsonA.size(); i++ ) {
						JsonObject jsob= (JsonObject) jsonA.get(i);
						String trip_id = ".";
						if ( jsob.get("trip_id") != null )
						{ trip_id = jsob.get("trip_id").getAsString(); }

						String taxi_id = ",";
						if ( jsob.get("taxi_id") != null )
						{ taxi_id = jsob.get("taxi_id").getAsString(); }

						String trip_start_timestamp = ";";
						if ( jsob.get("trip_start_timestamp") != null )
						{ trip_start_timestamp = jsob.get("trip_start_timestamp").getAsString(); }
						
						String trip_end_timestamp = ";";
						if ( jsob.get("trip_end_timestamp") != null )
						{ trip_end_timestamp = jsob.get("trip_end_timestamp").getAsString(); }

						int trip_seconds = 0;
						if ( jsob.get("trip_seconds") != null )
						{ trip_seconds = jsob.get("trip_seconds").getAsInt(); }

						int pickup_community_area = 0;
						if ( jsob.get("pickup_community_area") != null )
						{ pickup_community_area = jsob.get("pickup_community_area").getAsInt(); }

						int dropoff_community_area = 0;
						if ( jsob.get("dropoff_community_area") != null )
						{ dropoff_community_area = jsob.get("dropoff_community_area").getAsInt(); }

						double trip_miles = 0.0;
						if ( jsob.get("trip_miles") != null )
						{ trip_miles = jsob.get("trip_miles").getAsDouble(); }

						double trip_total = 0.0;
						if ( jsob.get("trip_total") != null )
						{ trip_total = jsob.get("trip_total").getAsDouble(); }

						double pickup_centroid_latitude = 0.0;
						if ( jsob.get("pickup_centroid_latitude") != null )
						{ pickup_centroid_latitude = jsob.get("pickup_centroid_latitude").getAsDouble(); }

						double pickup_centroid_longitude = 0.0;
						if ( jsob.get("pickup_centroid_longitude") != null )
						{ pickup_centroid_longitude = jsob.get("pickup_centroid_longitude").getAsDouble(); }

						String company = "..";
						if ( jsob.get("company") != null )
						{ company = jsob.get("company").getAsString(); }

						Servicio servicio = new Servicio( trip_id, taxi_id, trip_start_timestamp, trip_end_timestamp, trip_seconds, pickup_community_area, dropoff_community_area, trip_miles, trip_total, pickup_centroid_latitude, pickup_centroid_longitude );
						Taxi taxi = new Taxi( taxi_id, company );
						serviciosCargados.add( servicio );
						taxisCargados.add( taxi );
						
						latitudRef = latitudRef+pickup_centroid_latitude;
						longitudRef = longitudRef+pickup_centroid_longitude;
						
						if(trip_miles>0.0) {
							if(!millaServicios.contiene(trip_miles)) {
								Lista<Servicio> serviciosMilla = new Lista<>();
								serviciosMilla.add(servicio);
								millaServicios.insertar(trip_miles, serviciosMilla);
							}
							else {
								millaServicios.dar(trip_miles).add(servicio);
							}
						}
						
						if(pickup_community_area > 0 && dropoff_community_area >0 && !trip_start_timestamp.equals("..")) {
							String zonaI = Integer.toString(pickup_community_area);
							String zonaT = Integer.toString(dropoff_community_area);
							String zonazona = zonaI+"-"+zonaT;
							if(!rangoZonaServicios.contiene(zonazona)) {
								ArbolRojoNegro<String, Lista<Servicio>> serviciosEntreZonas = new ArbolRojoNegro<>();
								Lista<Servicio> servicioszona = new Lista<>();
								servicioszona.add(servicio);
								serviciosEntreZonas.insertar(trip_start_timestamp, servicioszona );
								rangoZonaServicios.insertar(zonazona, serviciosEntreZonas);
							}
							else if(rangoZonaServicios.contiene(zonazona) && !rangoZonaServicios.dar(zonazona).contiene(trip_start_timestamp)) {
								Lista<Servicio> servicioszona = new Lista<>();
								servicioszona.add(servicio);
								rangoZonaServicios.dar(zonazona).insertar(trip_start_timestamp, servicioszona);
							}
							else {
								rangoZonaServicios.dar(zonazona).dar(trip_start_timestamp).add(servicio);
							}
						}
						
					}
				}

				Iterator<Taxi> iterator1 = taxisCargados.iterator();
				while( iterator1.hasNext()) {
					Taxi taxi = iterator1.next();
					String compania = taxi.getCompany();
					String idtaxi = taxi.getTaxiId();
					if(compania.equals("..")) {
						if(!companiaTaxis.contiene("Independent Owner")) {
							Lista<String> taxisCompania = new Lista<>();
							taxisCompania.add(idtaxi);
							companiaTaxis.insertar("Independent Owner", taxisCompania);
						}
						else {
							if(companiaTaxis.dar("Independent Owner").buscar(idtaxi) == null) {

								companiaTaxis.dar("Independent Owner").add(idtaxi);
							}
						}
					}
					else {
						if(!companiaTaxis.contiene(compania)) {
							Lista<String> taxisCompania = new Lista<>();
							taxisCompania.add(idtaxi);
							companiaTaxis.insertar(compania, taxisCompania);
						}
						else {
							if(companiaTaxis.dar(compania).buscar(idtaxi) == null) {
								companiaTaxis.dar(compania).add(idtaxi);
							}
						}
					}
				}
				
				Iterator<Servicio> iterator2 = serviciosCargados.iterator();
				while(iterator2.hasNext()) {
					Servicio servicio = iterator2.next();
					int zonaInicio =  servicio.getPickupZone();
					if(!zonaInicioServicios.contiene(zonaInicio)) {
						Lista<Servicio> serviciosArea = new Lista<>();
						serviciosArea.add(servicio);
						zonaInicioServicios.insertar(zonaInicio, serviciosArea);
					}
					else {
						zonaInicioServicios.dar(zonaInicio).add(servicio);
					}
				}
				
				latitudRef = latitudRef/serviciosCargados.size();
				longitudRef = longitudRef/serviciosCargados.size();
				
				while( iterator2.hasNext()) {
					double lat = iterator2.next().getPickupLatitud();
					double lon = iterator2.next().getPickupLongitud();
					final int R = 6371*1000; // Radious of the earth in meters
					Double latDistance = Math.toRadians(latitudRef-lat);
					Double lonDistance = Math.toRadians(longitudRef-lon);
					Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat))* Math.cos(Math.toRadians(latitudRef)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
					Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
					Double distance = R * c;
				}

				System.out.println(latitudRef);
				System.out.println(longitudRef);
				}

			catch (JsonIOException e1 ) {
				e1.printStackTrace();
			}
			catch (JsonSyntaxException e2) {
				e2.printStackTrace();
			}
			catch (FileNotFoundException e3) {
				e3.printStackTrace();
			}
			return false;
		}
		else {
			try {
				JsonArray jsonA= (JsonArray) parser.parse(new FileReader(direccionJson));

				for( int i =0; jsonA != null && i < jsonA.size(); i++ ) {
					JsonObject jsob= (JsonObject) jsonA.get(i);
					String trip_id = ".";
					if ( jsob.get("trip_id") != null )
					{ trip_id = jsob.get("trip_id").getAsString(); }

					String taxi_id = ",";
					if ( jsob.get("taxi_id") != null )
					{ taxi_id = jsob.get("taxi_id").getAsString(); }

					String trip_start_timestamp = ";";
					if ( jsob.get("trip_start_timestamp") != null )
					{ trip_start_timestamp = jsob.get("trip_start_timestamp").getAsString(); }
					
					String trip_end_timestamp = ";";
					if ( jsob.get("trip_end_timestamp") != null )
					{ trip_end_timestamp = jsob.get("trip_end_timestamp").getAsString(); }

					int trip_seconds = 0;
					if ( jsob.get("trip_seconds") != null )
					{ trip_seconds = jsob.get("trip_seconds").getAsInt(); }

					int pickup_community_area = 0;
					if ( jsob.get("pickup_community_area") != null )
					{ pickup_community_area = jsob.get("pickup_community_area").getAsInt(); }

					int dropoff_community_area = 0;
					if ( jsob.get("dropoff_community_area") != null )
					{ dropoff_community_area = jsob.get("dropoff_community_area").getAsInt(); }

					double trip_miles = 0.0;
					if ( jsob.get("trip_miles") != null )
					{ trip_miles = jsob.get("trip_miles").getAsDouble(); }

					double trip_total = 0.0;
					if ( jsob.get("trip_total") != null )
					{ trip_total = jsob.get("trip_total").getAsDouble(); }

					double pickup_centroid_latitude = 0.0;
					if ( jsob.get("pickup_centroid_latitude") != null )
					{ pickup_centroid_latitude = jsob.get("pickup_centroid_latitude").getAsDouble(); }

					double pickup_centroid_longitude = 0.0;
					if ( jsob.get("pickup_centroid_longitude") != null )
					{ pickup_centroid_longitude = jsob.get("pickup_centroid_longitude").getAsDouble(); }

					String company = "..";
					if ( jsob.get("company") != null )
					{ company = jsob.get("company").getAsString(); }

					Servicio servicio = new Servicio( trip_id, taxi_id, trip_start_timestamp, trip_end_timestamp, trip_seconds, pickup_community_area, dropoff_community_area, trip_miles, trip_total, pickup_centroid_latitude, pickup_centroid_longitude );
					Taxi taxi = new Taxi( taxi_id, company );
					serviciosCargados.add( servicio );
					taxisCargados.add( taxi );
					
					latitudRef = latitudRef+pickup_centroid_latitude;
					longitudRef = longitudRef+pickup_centroid_longitude;
					
					if(trip_miles>0.0) {
						if(!millaServicios.contiene(trip_miles)) {
							Lista<Servicio> serviciosMilla = new Lista<>();
							serviciosMilla.add(servicio);
							millaServicios.insertar(trip_miles, serviciosMilla);
						}
						else {
							millaServicios.dar(trip_miles).add(servicio);
						}
					}
					
					if(pickup_community_area > 0 && dropoff_community_area >0 && !trip_start_timestamp.equals("..")) {
						String zonaI = Integer.toString(pickup_community_area);
						String zonaT = Integer.toString(dropoff_community_area);
						String zonazona = zonaI+"-"+zonaT;
						if(!rangoZonaServicios.contiene(zonazona)) {
							ArbolRojoNegro<String, Lista<Servicio>> serviciosEntreZonas = new ArbolRojoNegro<>();
							Lista<Servicio> servicioszona = new Lista<>();
							servicioszona.add(servicio);
							serviciosEntreZonas.insertar(trip_start_timestamp, servicioszona );
							rangoZonaServicios.insertar(zonazona, serviciosEntreZonas);
						}
						else if(rangoZonaServicios.contiene(zonazona) && !rangoZonaServicios.dar(zonazona).contiene(trip_start_timestamp)) {
							Lista<Servicio> servicioszona = new Lista<>();
							servicioszona.add(servicio);
							rangoZonaServicios.dar(zonazona).insertar(trip_start_timestamp, servicioszona);
						}
						else {
							rangoZonaServicios.dar(zonazona).dar(trip_start_timestamp).add(servicio);
						}
					}
					
				}	

				Iterator<Taxi> iterator1 = taxisCargados.iterator();
				while( iterator1.hasNext()) {
					Taxi taxi = iterator1.next();
					String compania = taxi.getCompany();
					String idtaxi = taxi.getTaxiId();
					if(compania.equals("..")) {
						if(!companiaTaxis.contiene("Independent Owner")) {
							Lista<String> taxisCompania = new Lista<>();
							taxisCompania.add(idtaxi);
							companiaTaxis.insertar("Independent Owner", taxisCompania);
						}
						else {
							if(companiaTaxis.dar("Independent Owner").buscar(idtaxi) == null) {

								companiaTaxis.dar("Independent Owner").add(idtaxi);
							}
						}
					}
					else {
						if(!companiaTaxis.contiene(compania)) {
							Lista<String> taxisCompania = new Lista<>();
							taxisCompania.add(idtaxi);
							companiaTaxis.insertar(compania, taxisCompania);
						}
						else {
							if(companiaTaxis.dar(compania).buscar(idtaxi) == null) {
								companiaTaxis.dar(compania).add(idtaxi);
							}
						}
					}
				}
				
				Iterator<Servicio> iterator2 = serviciosCargados.iterator();
				while(iterator2.hasNext()) {
					Servicio servicio = iterator2.next();
					int zonaInicio =  servicio.getPickupZone();
					if(!zonaInicioServicios.contiene(zonaInicio)) {
						Lista<Servicio> serviciosArea = new Lista<>();
						serviciosArea.add(servicio);
						zonaInicioServicios.insertar(zonaInicio, serviciosArea);
					}
					else {
						zonaInicioServicios.dar(zonaInicio).add(servicio);
					}
				}
				
				latitudRef = latitudRef/serviciosCargados.size();
				longitudRef = longitudRef/serviciosCargados.size();
				
				while( iterator2.hasNext()) {
					double lat = iterator2.next().getPickupLatitud();
					double lon = iterator2.next().getPickupLongitud();
					final int R = 6371*1000; // Radious of the earth in meters
					Double latDistance = Math.toRadians(latitudRef-lat);
					Double lonDistance = Math.toRadians(longitudRef-lon);
					Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat))* Math.cos(Math.toRadians(latitudRef)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
					Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
					Double distance = R * c;
				}

				System.out.println(latitudRef);
				System.out.println(longitudRef);
			}

			catch (JsonIOException e1 ) {
				e1.printStackTrace();
			}
			catch (JsonSyntaxException e2) {
				e2.printStackTrace();
			}
			catch (FileNotFoundException e3) {
				e3.printStackTrace();
			}
			return false;
			
		}
		
	}

	@Override
	public IList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		Lista<TaxiConServicios> taxiConServicios = new Lista<TaxiConServicios>();
		Lista<TaxiConServicios> listaAuxiliar = new Lista<>();
		int mayorServicios = 0;
		Iterator<String> iterator = companiaTaxis.dar(compania).iterator();
		while(iterator.hasNext()) {
			String taxiID = iterator.next();
			TaxiConServicios taxiConS = new TaxiConServicios(taxiID, compania);
			Iterator<Servicio> iterator2 = zonaInicioServicios.dar(zonaInicio).iterator();
			while(iterator2.hasNext()) {
				Servicio ser = iterator2.next();
				if(taxiID.equals(ser.getTaxiId())) {
					taxiConS.getServicios().add(ser);
				}
			}
			listaAuxiliar.add(taxiConS);
			if(taxiConS.getServicios().size()>mayorServicios) {
				mayorServicios = taxiConS.getServicios().size();
			}
		}
		Iterator<TaxiConServicios> iter = listaAuxiliar.iterator();
		while(iter.hasNext()) {
			TaxiConServicios taxiServicios = iter.next();
			if(taxiServicios.getServicios().size() == mayorServicios) {
				taxiConServicios.add(taxiServicios);
			}
		}
		return taxiConServicios;
	}


	@Override
	public IList<Servicio> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		Lista<Servicio> serviciosDistancia = new Lista<>();
		for(double i = distanciaMinima; i <= distanciaMaxima; i+=0.1 ) {
			if(millaServicios.contiene(i)) {
				Iterator<Servicio> iterator = millaServicios.dar(i).iterator();
				while(iterator.hasNext()) {
					serviciosDistancia.add(iterator.next());
				}
			}
		}	
		return serviciosDistancia;
	}


	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		Lista<Servicio> serviciosZonaTiempo = new Lista<>();
		String zonaI = Integer.toString(zonaInicio);
		String zonaT = Integer.toString(zonaFinal);
		String zonazona = zonaI+"-"+zonaT;
		String fecha1 = fechaI+"T"+horaI;
		String fecha2 = fechaF+"T"+horaF;
		Iterator<Servicio> iterator = rangoZonaServicios.dar(zonazona).dar(fecha1).iterator();
		while(iterator.hasNext()) {
			String fechaFF = iterator.next().getEndtTime();
			if(fechaFF.compareTo(fecha2)<0 || fechaFF.compareTo(fecha2)==0) {
				serviciosZonaTiempo.add(iterator.next());
			}
		}
		return serviciosZonaTiempo;
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}

	@Override
	public IList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}
}
